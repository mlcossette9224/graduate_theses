## Chapter 01 subset VCF
## sampling of VCF file of 40 individuals within four groups

root=/home/ckessler/projects/rrg-shaferab/ckessler/
datadir=/home/ckessler/projects/rrg-shaferab/ckessler/Analysis_test/demo_data
CH01=/home/ckessler/projects/rrg-shaferab/ckessler/chapter01

cd $CH01
# create sample info file from CH01_sampling.R with nano, saved as CH01_Samples.txt
# create sampleIDs list
cat CH01_Samples.txt | awk  -F"\t" '{print $1}' > CH01_samplesIDs.txt

## filter vcf file
module load vcftools
vcftools --gzvcf $datadir/deer_angsd_renamed.vcf.gz --keep CH01_samplesIDs.txt --out deer_angsd_CH01_samples.vcf.gz

