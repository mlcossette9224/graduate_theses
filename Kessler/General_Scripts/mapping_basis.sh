# Script for the mapping of Deer samples in Graham
# template for future mappings
# From FastQC to ...

##########################################################################################################
#	SETUP
##########################################################################################################
# path to fastq
fastq=/home/ckessler/projects/rrg-shaferab/ckessler/...

# path to references
ref=/home/ckessler/projects/rrg-shaferab/ckessler/...

# path to analysis directory
analysisdir=/home/ckessler/projects/rrg-shaferab/ckessler/...

# root
root=/home/ckessler/projects/rrg-shaferab/ckessler



##########################################################################################################
#	PREP 
##########################################################################################################

# index the genome
sbatch -o indexing.out $root/03_index.sh $ref # you can find the adapted 03_index.sh in /Users/camillekessler/Desktop/PhD/Scripts/adapted_Resequencing_SOP

# going to split up the genome into scaffolds to help with run - do before submitting job
mkdir $analysisdir/mpileup_run
awk 'BEGIN {FS="\t"}; {print $1 FS "0" FS $2}' $ref/wtdgenome1.fasta.fai > mpileup_run/mpileup_coord.bed
awk 'BEGIN {FS="\t"}; {print $1 FS "1" FS $2}' $ref/wtdgenome1.fasta.fai > mpileup_run/mpileup_coord.list

# Store the slurms in different directories:
mkdir $analysisdir/fastqc_slurm 
mkdir $analysisdir/trimmed_slurm
mkdir $analysisdir/align_slurm
mkdir $analysisdir/dups_slurm
mkdir $analysisdir/unique_slurm
mkdir $analysisdir/clean_slurm

# get sambamba
wget https://github.com/biod/sambamba/releases/download/v0.7.0/sambamba-0.7.0-linux-static.gz
gunzip sambamba-0.7.0-linux-static.gz

##########################################################################################################
#	FastQC 
##########################################################################################################
cd $analysisdir
module load StdEnv/2016.4
module load fastqc
for f in `ls $fastq/*.fastq.gz`
do

ID=$(echo ${f} | sed "s|$fastq/||" | sed 's/_R[0-9].*//')
analysisdir=/home/ckessler/projects/rrg-shaferab/ckessler/...

echo ${f}
echo ${ID}
sbatch -o $analysisdir/fastqc_slurm/${ID}-%A.out --account=rrg-shaferab --time=02:00:00 --job-name=fastq --wrap="fastqc ${f} -o ${analysisdir}"
sleep 10
done




##########################################################################################################
#	Trimmomatic 
##########################################################################################################
for f in $(ls $fastq/*.fastq.gz | sed "s|$fastq/||" | sed 's/_R[0-9].*//' | uniq)
do
echo ${f}
fastq=/home/ckessler/projects/rrg-shaferab/ckessler/...
analysisdir=/home/ckessler/projects/rrg-shaferab/ckessler/...

sbatch -o $analysisdir/trimmed_slurm/${f}-%A.out $root/02_trimmomaticPE.sh ${f} ${analysisdir} ${fastq}
sleep 10
done



##########################################################################################################
#	bwa_sort 
##########################################################################################################

for f in $(ls $fastq/*.fastq.gz |  sed "s|$fastq/||" | sed 's/_R[0-9].*//'| uniq) # change cut -f1-4 -d'_' didn't work with Odo_ON_X2_S12_L004_R1_001
do
echo ${f}

fastq=/home/ckessler/projects/rrg-shaferab/ckessler/...
ref=/home/ckessler/projects/rrg-shaferab/ckessler/...
analysisdir=/home/ckessler/projects/rrg-shaferab/ckessler/...

sbatch -o $analysisdir/align_slurm/${f}-%A.out $root/04_bwa-sortSE.sh ${f} ${ref}/wtdgenome1.fasta ${fastq} ${analysisdir}
sleep 10
done


##########################################################################################################
#	picard
##########################################################################################################

for f in $(ls *.bam | sed 's/.sorted_reads.bam//' | sort -u)
do
echo ${f}
analysisdir=/home/ckessler/projects/rrg-shaferab/ckessler/...
root=/home/ckessler/projects/rrg-shaferab/ckessler

sbatch -o $analysisdir/dups_slurm/${f}-%A.out $root/05_picard-dup.sh ${f} ${analysisdir} ${root}
sleep 10
done

##########################################################################################################
#	Unique
##########################################################################################################

for f in $(ls *.deduped_reads.bam | sed 's/.deduped_reads.bam//' | sort -u)
do
echo ${f}
analysisdir=/home/ckessler/projects/rrg-shaferab/ckessler/...
root=/home/ckessler/projects/rrg-shaferab/ckessler

sbatch -o $analysisdir/unique_slurm/${f}-%A.out $root/06_unique.sh ${f} ${analysisdir} ${root}
sleep 10
done


##########################################################################################################
#	realign
##########################################################################################################

for f in $(ls *.unique_reads.bam | sed 's/.unique_reads.bam//' | sort -u)
do
echo ${f}

ref=/home/ckessler/projects/rrg-shaferab/ckessler/GENOME_files
analysisdir=/home/ckessler/projects/rrg-shaferab/ckessler/...
root=/home/ckessler/projects/rrg-shaferab/ckessler

sbatch -o $analysisdir/clean_slurm/${f}-%A.out $root/07_realign.sh ${f} ${ref} ${analysisdir} ${root}

sleep 10
done


########
# check, should be 20 each
########
for f in $(ls *.unique_reads.bam | sed 's/.unique_reads.bam//' | sort -u)
do 

echo ${f}
ls ${f}* | wc

done

##########################################################################################################
#	Mosdepth
##########################################################################################################
# installation
wget https://github.com/brentp/mosdepth/releases/download/v0.3.1/mosdepth
chmod 764 mosdepth
./mosdepth -h

# final coverage
for f in $(ls *.realigned.bam | sed 's/.realigned.bam//' | sort -u)
do
echo ${f}

analysisdir=/home/ckessler/projects/rrg-shaferab/ckessler/...
root=/home/ckessler/projects/rrg-shaferab/ckessler

sbatch -o $analysisdir/mosdepth_slurm/${f}-%A.out --mem=4G --account=rrg-shaferab --time=02:00:00 --job-name=mosdepth --wrap="$root/mosdepth --no-per-base ${f} ${f}.realigned.bam"

sleep 10
done



##########################################################################################################
#	MultiQC
##########################################################################################################
# installation

module load python/3.6
virtualenv --no-download ~/ENV # create a virtual environment,
source ~/ENV/bin/activate # activate it
pip install --no-index --upgrade pip # upgrade pip
pip install multiqc # install 
deactivate # exit environment
/home/ckessler/ENV/bin/multiqc -f .


rsync -rv ckessler@graham.computecanada.ca:/home/ckessler/projects/rrg-shaferab/ckessler/.../multiqc* /Users/camillekessler/Desktop/Dropbox/PhD/Data



##########################################################################################################
#	ANGSD
# for all samples, not just this batch
##########################################################################################################
root=/home/ckessler/projects/rrg-shaferab/ckessler
analysisdir=/home/ckessler/projects/rrg-shaferab/ckessler/Analysis_test

ls $root/mapping_B*/*realigned.bam > bam_list 

sbatch -o $analysisdir/ANGSD.out $root/08_ANGSD.sh

#Editing angsd vcf to be compatable with further steps and vcftools we need a new header called vcf_head
#We change from ##fileformat=VCFv4.2(angsd version) to ##fileformat=VCFv4.2
module load StdEnv/2018.3 

gunzip -c $analysisdir/demo_data/deer_angsd.vcf.gz  | head -n 1


wget https://faculty.washington.edu/browning/beagle/beagle.27Jan18.7e1.jar



sbatch -o plink.out --cpus-per-task 4 --mem 16G --account=rrg-shaferab --time=6-00:00 --job-name=plink --wrap="java -Xmx8g -Djava.awt.headless=true -jar beagle.27Jan18.7e1.jar gtgl=/home/ckessler/projects/rrg-shaferab/ckessler/Analysis_test/demo_data/deer_angsd.vcf.gz impute=false out=demo_data/deer_mod_angsd.vcf.gz"
# 49414154





echo "##fileformat=VCFv4.2" > header.vcf

#insert new first line with: ##fileformat=VCFv4.2

sbatch -o format.out --mem=4G --account=rrg-shaferab --time=04:00:00 --job-name=format --wrap="zcat demo_data/deer_angsd.vcf.gz | sed 1d  > temp.vcf.gz" # actually does not keep zipping

sbatch -o format.out --mem=4G --account=rrg-shaferab --time=04:00:00 --job-name=format --wrap="cat header.vcf temp.vcf.gz > demo_data/deer_angsd_vcfHeader.vcf"

sbatch -o zip.out --cpus-per-task 4 --mem 16GB --account=rrg-shaferab --time=15:00:00 --job-name=zip --wrap="gzip demo_data/deer_angsd_vcfHeader.vcf"
# 49201558

## rename individuals in VCF --> from IND01 to Odo_,..
module load StdEnv/2018.3 
module load bcftools
cd $analysisdir

cat bam_list | awk -F/ '$0=$NF' | sed 's/_[^_]*//2g' > Samples_list.txt # list with order of samples IDs

# rehaeder in bcftools
bcftools reheader -s $analysisdir/Samples_list.txt $analysisdir/demo_data/deer_angsd_vcfHeader.vcf.gz -o $analysisdir/demo_data/deer_angsd_renamed.vcf.gz 

# check that it worked
bcftools query -l $analysisdir/demo_data/deer_angsd_renamed.vcf.gz
gunzip -c $analysisdir/demo_data/deer_angsd_renamed.vcf.gz  | head -n 10 | cut -f 1-10 | column -t # also works with vcf








# Editing angsd vcf to be compatable with further steps and vcftools we need a new header called vcf_head
# We change from ##fileformat=VCFv4.2(angsd version) to ##fileformat=VCFv4.2
# echo "##fileformat=VCFv4.2" > header.txt
# 
# module load nixpkgs/16.09
# module load intel/2018.3
# module load StdEnv/2018.3 
# module load tabix
# 
# sbatch -o format.out --mem=4G --account=rrg-shaferab --time=02:00:00 --job-name=format --wrap="tabix -r header.txt deer_angsd.vcf.gz > deer_angsd_comp.vcf.gz"
# 
# 
# 













