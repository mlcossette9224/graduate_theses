#!/bin/bash
#SBATCH --account=rrg-shaferab
#SBATCH --job-name=angsd
#SBATCH --mem 256G
#SBATCH --cpus-per-task 16
#SBATCH --time=00-70:00

module load StdEnv/2016.4
module load angsd
angsd -bam bam_list -doMajorMinor 1 -domaf 1 -out deer_angsd -doVcf 1 -nThreads 16 -doGeno 4 -doPost 1 -gl 2 -SNP_pval 1e-6 -minMapQ 20 -minQ 20 -doCounts 1 -skipTriallelic 1 -doGlf 2
#note the VCF output is 4.2 and can't be read by vcftools as is; continuing through until phasing will produce a readable VCF

