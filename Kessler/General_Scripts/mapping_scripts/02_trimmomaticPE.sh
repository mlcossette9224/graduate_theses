#!/bin/bash
#SBATCH --account=rrg-shaferab
#SBATCH --job-name=trimmomatic
#SBATCH --cpus-per-task=16
#SBATCH --mem=8G
#SBATCH --time=0-06:00 # time (DD-HH:MM)

module load StdEnv/2016.4
module load trimmomatic

java -jar $EBROOTTRIMMOMATIC/trimmomatic-0.36.jar PE -threads 16 -phred33 ${3}/${1}_R1_001.fastq.gz ${3}/${1}_R2_001.fastq.gz  ${2}/${1}_trim_R1_001.fastq.gz ${2}/${1}_NAtrim_R1_001.fastq.gz ${2}/${1}_trim_R2_001.fastq.gz ${2}/${1}_NAtrim_R2_001.fastq.gz ILLUMINACLIP:TruSeq3-PE.fa:2:30:10 LEADING:3 TRAILING:3 SLIDINGWINDOW:4:15 MINLEN:36

